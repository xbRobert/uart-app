package com.DeviceManager.jni;
import java.util.ArrayList;

class Command {
	final static int UNKNOWN_ID = -1;

	//命令的ID标识了一个设备支持的命令
	private int ID;
	// 写入设备或读出设备多少字节
	private int dataLen;
	// 与命令有关，如果为写命令则data为要写的值，如果为读命令则data代表读回来的值，如果命令不需要参数则为空
	private byte[] data;

	public Command() {
		this.ID = UNKNOWN_ID;
		this.dataLen = 0;
		this.data = null;
	}

	// 设置这个命令的ID
	public void setID(int ID) {
		this.ID = ID;
	}

	// 设置命令所需的data
	public void setData(byte[] data) {
		this.data = data;
	}

	// 设置成功传输了多少字节, 不应该被用户调用，应该被DeviceManager调用
	public void setDataLen(int datalen) {
		this.dataLen = dataLen;
	}

	// 获取成功传输了多少字节数据
	public int  getDataLen() {
		return dataLen;
	}
}

class Device {
	private String name;
	private int addrh;
	private int addrl;

	public String getDeviceName() {
		return name;
	}
}


public class DeviceManager {
	private static DeviceManager instance;
	private ArrayList<Device> deviceList;
	private DeviceManager() {
		System.out.println("DeviceManager() called");
	}


	// for application getting DeviceManager
	public static DeviceManager getInstance() {
		if(null == instance) {
			instance = new DeviceManager();
		}

		return instance;
	}

	// get all connected to system devices, return in a list 
	public ArrayList<Device> getAllDevices() {
		return deviceList;
	}

	// get one device's name, like light%d, %d is which device of this kind.
	public String  getDeviceName(Device device) {
		return device.getDeviceName();
	}

	// send a command to device, return command status, timeout, lost connect....
	public native int sendCommand(Device device, Command command); 
}
