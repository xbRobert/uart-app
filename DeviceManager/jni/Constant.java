
// define all constance DeviceManager needed

class Constant {
	// device id
	public final int AIR_CONDITIONER_ID = 0x0;
	public final int LIGHT_ID = 0x1;
	public final int VOICE_CONTROLER_ID = 0x2;
	public final int CAMERA_ID = 0x3;
	public final int PM_ID = 0x4;
	public final int AIR_CLEANER_ID = 0x5;

	// command id
	public final int LIGHT_ON = makeCommand(LIGHT_ID, 0x0);
	public final int LIGHT_OFF = makeCommand(LIGHT_ID, 0x1);

	// device type name
	public final String LIGHT_NAME = "light";
	public final String AIR_CLEANER_NAME = "air_cleaner";
	public final String VOICE_CONTROLER_NAME = "voice_controler";

	private int makeCommand(int device_type, int command_index) {
		return (device_type << 16) | command_index;
	}
}
