COMPILER=
#COMPILER=arm-linux-androideabi-
#COMPILER=/home/robert/my-arm-none-linux-gnueabi-i686-pc-linux-gnu/arm-2009q1/bin/arm-none-linux-gnueabi-

CC=${COMPILER}gcc
TARGET=uart-app
${TARGET}:main.o uart.o
	${CC} -static -o ${TARGET} main.o uart.o -lpthread
main.o:main.c
	${CC} -Wall -c main.c -o main.o
uart.o:uart.c uart.h
	${CC} -Wall -c uart.c -o uart.o
clean:
	rm ${TARGET} *.o
